import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.red,
        body: SafeArea(
          child: Container(
            color: Colors.green,
            height: 111.0,
            width: 222.0,
            margin: EdgeInsets.symmetric(vertical: 50.0, horizontal: 10.0),
            // padding: EdgeInsets.only(left: 10),
            padding: EdgeInsets.fromLTRB(10, 20, 10, 20),
            child: Text("Hello There"),
          ),
        ),
      ),
    );
  }
}
